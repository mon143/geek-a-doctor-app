import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Login-Page.dart';
import 'package:geekdoctor/Pages/User-Service-Page/Booking-Client-Page.dart';
import 'package:geekdoctor/Pages/User-Service-Page/User-Service-Available-Time.dart';
import 'package:geekdoctor/Pages/User-Service-Page/User-Service-History.dart';
import 'package:geekdoctor/Pages/User-Service-Page/User-Service-Profile-Page.dart';

//import '../routes.dart';

class UserServiceDrawer extends StatefulWidget {
  const UserServiceDrawer({Key? key}) : super(key: key);

  @override
  _UserServiceDrawerState createState() => _UserServiceDrawerState();
}

class _UserServiceDrawerState extends State<UserServiceDrawer> {
  User? user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection("table-user-service")
          .where('email', isEqualTo: user!.email)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot?> snapshot) {
        final currentUser = snapshot.data?.docs;

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        return SingleChildScrollView(
          child: Container(
            width: 300.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            padding: EdgeInsets.only(top: 50, bottom: 70, left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Hero(
                      tag: "tag1",
                      child: GestureDetector(
                        onTap: () {
                          //Routing.goToPage(UserServiceProfilePage(), context);
                        },
                        child: CircleAvatar(
                          radius: 30.0,
                          backgroundImage: NetworkImage("${currentUser![0]['imageUrl']}"),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${currentUser[0]['fullName']}",
                            style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.bold),
                          ),
                          Icon(
                            Icons.circle,
                            size: 10,
                            color: Colors.green,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Text("Profile"),
                  leading: IconButton(
                    icon: Icon(Icons.account_circle),
                    color: Colors.orange,
                    onPressed: () {
                      //Navigator.popAndPushNamed(context, '/user-service-profile-page');
                    },
                  ),
                  onTap: () {
                    //Navigator.of(context).pop();
                    Navigator.pushNamed(context, '/user-service-profile-page');
                    //Routing.goToPage(UserServiceProfilePage(), context);
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Text("Booking List"),
                  leading: IconButton(
                    icon: Icon(Icons.three_p),
                    color: Colors.blue,
                    onPressed: () {
                      //Navigator.pushNamed(context, '/booking-client-page');
                    },
                  ),
                  onTap: () {
                    // Navigator.of(context).pop();
                    Navigator.pushNamed(context, '/booking-client-page');
                    //Routing.goToPage(BookingClientPage(), context);
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Text("History"),
                  leading: IconButton(
                    icon: Icon(Icons.history),
                    color: Colors.green,
                    onPressed: () {},
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => UserServiceHistory()));
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                // ListTile(
                //   title: Text("Time Available/UnAvailable"),
                //   leading: IconButton(
                //     icon: Icon(Icons.history),
                //     color: Colors.green,
                //     onPressed: () {},
                //   ),
                //   onTap: () {
                //     Navigator.push(context,
                //         MaterialPageRoute(builder: (context) => CalendarBooking()));
                //   },
                // ),
                Divider(
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 300.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    // Icon(
                    //   Icons.settings,
                    //   color: Colors.black,
                    // ),
                    // SizedBox(
                    //   width: 10,
                    // ),
                    // Text(
                    //   'Settings',
                    //   style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                    // ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 2,
                      height: 20,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    ActionChip(
                        label: Text("  Logout  "),
                        onPressed: () {
                          logout(context);
                          FirebaseFirestore.instance
                              .collection('table-user-service')
                              .doc(user!.uid)
                              .update({
                            "status": "Offline",
                          }).then((_) async {});
                        }),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();

    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => UserLoginClientPage()));
  }
}
