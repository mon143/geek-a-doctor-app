import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geekdoctor/Pages/User-Client-Page/Booking-List.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Login-Page.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:provider/src/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import '../routes.dart';

class UserClientDrawer extends StatefulWidget {
  const UserClientDrawer({Key? key}) : super(key: key);

  @override
  _UserClientDrawerState createState() => _UserClientDrawerState();
}

class _UserClientDrawerState extends State<UserClientDrawer> {
  User? user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection("table-user-client")
          .where('email', isEqualTo: user!.email)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot?> snapshot) {
        final currentUser = snapshot.data?.docs;

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: const CircularProgressIndicator());
        }
        return SingleChildScrollView(
          child: Container(
            width: 300.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            padding: EdgeInsets.only(top: 50, bottom: 70, left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Hero(
                      tag: "tag1",
                      child: CircleAvatar(
                        radius: 30.0,
                        backgroundImage: NetworkImage("${currentUser![0]['imageUrl']}"),
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${currentUser[0]['fullName']}",
                            style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.bold),
                          ),
                          Icon(
                            Icons.circle,
                            size: 10,
                            color: Colors.green,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Text("Profile"),
                  leading: IconButton(
                    icon: Icon(Icons.account_circle),
                    color: Colors.orange,
                    onPressed: () {
                      Navigator.pushNamed(context, '/user-client-profile-page');
                    },
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/user-client-profile-page');

                    //Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  title: Text("Booking List"),
                  leading: IconButton(
                    icon: Icon(Icons.three_p),
                    color: Colors.blue,
                    onPressed: () {
                      //Navigator.pushNamed(context, '/booking-list-page');
                    },
                  ),
                  onTap: () {
                    // Navigator.pushAndRemoveUntil(
                    //     context,
                    //     MaterialPageRoute(builder: (context) => BookingListPage()),
                    //     ModalRoute.withName("/booking-list-page"));
                    //Routing.goToPage(BookingListPage(), context);
                    //Navigator.pushNamed(context, '/booking-list-page');
                    Navigator.popAndPushNamed(context, '/booking-list-page');

                    //Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 300.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    ActionChip(
                        label: Text("  Logout  "),
                        onPressed: () async {
                          logout(context);
                          FirebaseFirestore.instance
                              .collection('table-user-client')
                              .doc(user!.uid)
                              .update({
                            "status": "Offline",
                          }).then((_) async {});
                        }),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();

    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => UserLoginClientPage()));
  }
}
