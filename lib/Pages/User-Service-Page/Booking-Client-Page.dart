import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geekdoctor/Pages/Chat-Page/Chat-To-User-Client.dart';
import 'package:geekdoctor/Provider/Bokk-A-Geek/Book-A-Geek.dart';
import 'package:geekdoctor/Provider/Chat-Controller-Provider/Chat-Controller-Provider.dart';
import 'package:geekdoctor/model/book_model.dart';
import 'package:provider/src/provider.dart';
import 'package:intl/intl.dart';

class BookingClientPage extends StatefulWidget {
  const BookingClientPage({Key? key}) : super(key: key);

  @override
  _BookingClientPageState createState() => _BookingClientPageState();
}

class _BookingClientPageState extends State<BookingClientPage> {
  User? user = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        centerTitle: true,
        title: Text("List Of Booking Client"),
      ),
      body: SafeArea(
        child: StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection("table-book")
                .where('userServiceModel.uid', isEqualTo: user!.uid)
                .orderBy('dateToBook', descending: true)
                .snapshots(),
            builder: (context, AsyncSnapshot<QuerySnapshot?> snapshot) {
              // final currentUser = snapshot.data!.docs;
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: const CircularProgressIndicator());
              }
              if (snapshot.data!.docs.isNotEmpty) {
                return Column(
                  children: [
                    // AspectRatio(
                    //   aspectRatio: 3 / 2,
                    //   child: Container(
                    //     width: double.infinity,
                    //     decoration: BoxDecoration(
                    //       image: DecorationImage(
                    //           image: AssetImage("images/booking.png"), fit: BoxFit.cover),
                    //       boxShadow: <BoxShadow>[
                    //         BoxShadow(
                    //             color: Colors.black54,
                    //             blurRadius: 15.0,
                    //             offset: Offset(0.0, 0.75))
                    //       ],
                    //       color: Colors.white60,
                    //     ),
                    //     child: Center(child: Text("Service Booking Schedule")),
                    //   ),
                    // ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: snapshot.data?.docs.length,
                          itemBuilder: (context, index) {
                            final userClient = snapshot.data!.docs[index];
                            final DocumentSnapshot bookingData =
                                snapshot.data!.docs[index];

                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                margin: const EdgeInsets.only(bottom: 5),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [Colors.orangeAccent, Colors.black26],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.orange.withOpacity(0.4),
                                      blurRadius: 8,
                                      spreadRadius: 2,
                                      offset: Offset(4, 4),
                                    ),
                                  ],
                                  borderRadius: BorderRadius.all(Radius.circular(24)),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            ClipOval(
                                              child: CachedNetworkImage(
                                                width: 100,
                                                height: 100,
                                                fit: BoxFit.cover,
                                                imageUrl:
                                                    userClient.get('userModel.imageUrl'),
                                                progressIndicatorBuilder: (context, url,
                                                        downloadProgress) =>
                                                    CircularProgressIndicator(
                                                        value: downloadProgress.progress),
                                                errorWidget: (context, url, error) =>
                                                    Icon(
                                                  Icons.error,
                                                  size: 100,
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: 8),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: 250,
                                                  child: Text(
                                                    "${userClient.get('userModel.fullName')} ",
                                                    softWrap: true,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily: 'avenir',
                                                        fontSize: 18.0),
                                                  ),
                                                ),
                                                Container(
                                                  width: 250,
                                                  child: Text(
                                                    "${userClient.get('userModel.clientAddress')} ",
                                                    style: TextStyle(
                                                        color: Colors.orangeAccent,
                                                        fontFamily: 'avenir'),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    RichText(
                                      text: TextSpan(
                                          text: "Service Need : \n",
                                          style: TextStyle(
                                              fontSize: 16.0, color: Colors.black),
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: "${userClient.get('serviceNeed')}",
                                              style: TextStyle(
                                                  fontSize: 16.0, color: Colors.white),
                                            ),
                                          ]),
                                    ),
                                    SizedBox(
                                      height: 16,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        RichText(
                                          text: TextSpan(
                                              text: "Status: \n",
                                              style: TextStyle(
                                                  fontSize: 16.0, color: Colors.black),
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: "${userClient.get('status')}",
                                                  style: TextStyle(
                                                      fontSize: 16.0,
                                                      color: userClient.get('status') ==
                                                              "Pending"
                                                          ? Colors.red
                                                          : Colors.white),
                                                ),
                                              ]),
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              "${userClient.get('dateToBook')}",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontFamily: 'avenir',
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            Text(
                                              "${userClient.get('startTime')}",
                                              style: TextStyle(
                                                  color: Colors.orangeAccent,
                                                  fontFamily: 'avenir'),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Align(
                                      alignment: Alignment.bottomRight,
                                      child: IconButton(
                                          icon: Icon(
                                            Icons.arrow_circle_down_outlined,
                                            size: 40,
                                          ),
                                          color: Colors.white,
                                          onPressed: () {
                                            showModalBottomSheet(
                                                backgroundColor: Colors.white,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(20),
                                                        topRight: Radius.circular(20))),
                                                elevation: 20,
                                                context: context,
                                                builder: (context) {
                                                  return Column(
                                                    mainAxisSize: MainAxisSize.min,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets.all(8.0),
                                                        child: Material(
                                                          elevation: 5,
                                                          borderRadius:
                                                              BorderRadius.circular(30),
                                                          color: Colors.orange,
                                                          child: MaterialButton(
                                                              padding:
                                                                  EdgeInsets.fromLTRB(
                                                                      20, 15, 20, 15),
                                                              minWidth:
                                                                  MediaQuery.of(context)
                                                                      .size
                                                                      .width,
                                                              onPressed: () async {
                                                                await FirebaseFirestore
                                                                    .instance
                                                                    .collection(
                                                                        "table-book")
                                                                    .doc(bookingData.id)
                                                                    .update({
                                                                  'status': "In-Progress",
                                                                }).then((result) {
                                                                  Navigator.of(context)
                                                                      .pop();
                                                                  Fluttertoast.showToast(
                                                                      msg:
                                                                          "Booking Accepted ");
                                                                  print("Success!");
                                                                }).catchError((error) {
                                                                  print("Error!");
                                                                });
                                                              },
                                                              child: Text(
                                                                "Accept",
                                                                textAlign:
                                                                    TextAlign.center,
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors.white,
                                                                ),
                                                              )),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets.all(8.0),
                                                        child: Material(
                                                          elevation: 5,
                                                          borderRadius:
                                                              BorderRadius.circular(30),
                                                          color: Colors.blue,
                                                          child: MaterialButton(
                                                              padding:
                                                                  EdgeInsets.fromLTRB(
                                                                      20, 15, 20, 15),
                                                              minWidth:
                                                                  MediaQuery.of(context)
                                                                      .size
                                                                      .width,
                                                              onPressed: () {
                                                                Navigator.of(context)
                                                                    .pop();

                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder: (BuildContext
                                                                              context) =>
                                                                          ChatToUserClientPage(
                                                                              clientInfo:
                                                                                  BookModel.fromMap(
                                                                                      userClient.data())),
                                                                    ));

                                                                // Navigator.pushNamed(
                                                                //     context,
                                                                //     '/chat-to-user-client');
                                                                context
                                                                    .read<
                                                                        ChatControllerProvider>()
                                                                    .setUserClientID(
                                                                        userClient
                                                                            .get('id'));
                                                              },
                                                              child: Text(
                                                                "Send Message",
                                                                textAlign:
                                                                    TextAlign.center,
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors.white,
                                                                ),
                                                              )),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets.all(8.0),
                                                        child: Material(
                                                          elevation: 5,
                                                          borderRadius:
                                                              BorderRadius.circular(30),
                                                          color: Colors.red,
                                                          child: MaterialButton(
                                                              padding:
                                                                  EdgeInsets.fromLTRB(
                                                                      20, 15, 20, 15),
                                                              minWidth:
                                                                  MediaQuery.of(context)
                                                                      .size
                                                                      .width,
                                                              onPressed: () async {
                                                                // Navigator.of(context)
                                                                //     .pop();
                                                                // Navigator.pushNamed(
                                                                //     context,
                                                                //     '/edit-booking-page');
                                                                // context
                                                                //     .read<
                                                                //     BookAGeekProvider>()
                                                                //     .setBookingID(
                                                                //     userService
                                                                //         .get('id'));
                                                              },
                                                              child: Text(
                                                                "Cancel",
                                                                textAlign:
                                                                    TextAlign.center,
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors.white,
                                                                ),
                                                              )),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets.all(8.0),
                                                        child: Material(
                                                          elevation: 5,
                                                          borderRadius:
                                                              BorderRadius.circular(30),
                                                          color: Colors.green,
                                                          child: MaterialButton(
                                                              padding:
                                                                  EdgeInsets.fromLTRB(
                                                                      20, 15, 20, 15),
                                                              minWidth:
                                                                  MediaQuery.of(context)
                                                                      .size
                                                                      .width,
                                                              onPressed: () async {
                                                                await FirebaseFirestore
                                                                    .instance
                                                                    .collection(
                                                                        "table-history")
                                                                    .add({
                                                                  "clientName":
                                                                      userClient.get(
                                                                          'userModel.fullName'),
                                                                  "clientEmail":
                                                                      userClient.get(
                                                                          'userModel.email'),
                                                                  "clientImage":
                                                                      userClient.get(
                                                                          'userModel.imageUrl'),
                                                                  "userServiceName":
                                                                      userClient.get(
                                                                          'userServiceModel.fullName'),
                                                                  "userServiceEmail":
                                                                      user!.email,
                                                                  "serviceImage":
                                                                      userClient.get(
                                                                          'userServiceModel.imageUrl'),
                                                                  "status": "Finished",
                                                                  "dateFinished":
                                                                      DateFormat.yMMMMd()
                                                                          .format(DateTime
                                                                              .now()),
                                                                }).then((value) {
                                                                  FirebaseFirestore
                                                                      .instance
                                                                      .collection(
                                                                          'table-book')
                                                                      .doc(bookingData.id)
                                                                      .update({
                                                                    "status": "Finished",
                                                                  }).then((_) {
                                                                    //Fluttertoast.showToast(msg: "Image Save");
                                                                  });

                                                                  Fluttertoast.showToast(
                                                                      msg:
                                                                          "Transaction Completed");
                                                                });

                                                                // Navigator.of(context)
                                                                //     .pop();
                                                                // Navigator.pushNamed(
                                                                //     context,
                                                                //     '/edit-booking-page');
                                                                // context
                                                                //     .read<
                                                                //     BookAGeekProvider>()
                                                                //     .setBookingID(
                                                                //     userService
                                                                //         .get('id'));
                                                              },
                                                              child: Text(
                                                                "Finish Transaction",
                                                                textAlign:
                                                                    TextAlign.center,
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors.white,
                                                                ),
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                });
                                          }),
                                    ),
                                  ],
                                ),
                              ),
                            );

                            // return Column(
                            //   children: [
                            //     SizedBox(height: 20.0),
                            //     Container(
                            //       height: 240,
                            //       width: double.infinity,
                            //       decoration: BoxDecoration(
                            //         boxShadow: <BoxShadow>[
                            //           BoxShadow(
                            //               color: Colors.black54,
                            //               blurRadius: 15.0,
                            //               offset: Offset(0.0, 0.75))
                            //         ],
                            //         color: Colors.white60,
                            //       ),
                            //       child: Column(
                            //         children: [
                            //           Row(
                            //             children: [
                            //               Padding(
                            //                 padding: const EdgeInsets.all(8.0),
                            //                 child: Container(
                            //                   height: 80,
                            //                   width: 280,
                            //                   child: Row(
                            //                     children: [
                            //                       Container(
                            //                         height: 100,
                            //                         width: 100,
                            //                         decoration: BoxDecoration(
                            //                           image: DecorationImage(
                            //                             fit: BoxFit.fill,
                            //                             image: NetworkImage(
                            //                                 "${userClient.get('userModel.imageUrl')}"),
                            //                           ),
                            //                         ),
                            //                       ),
                            //                       Padding(
                            //                         padding: const EdgeInsets.all(8.0),
                            //                         child: Text(
                            //                             "${userClient.get('userModel.fullName')} "
                            //                             "\n${userClient.get('dateToBook')}\n ${userClient.get('startTime')}"),
                            //                       ),
                            //                     ],
                            //                   ),
                            //                 ),
                            //               ),
                            //             ],
                            //           ),
                            //           SizedBox(
                            //             height: 10,
                            //           ),
                            //           Padding(
                            //             padding: const EdgeInsets.only(left: 10),
                            //             child: Row(
                            //               children: [
                            //                 RichText(
                            //                   text: TextSpan(
                            //                       text: "Status: ",
                            //                       style: TextStyle(
                            //                           fontSize: 16.0,
                            //                           color: Colors.black),
                            //                       children: <TextSpan>[
                            //                         TextSpan(
                            //                           text: "${userClient.get('status')}",
                            //                           style: TextStyle(
                            //                               fontSize: 16.0,
                            //                               color:
                            //                                   userClient.get('status') ==
                            //                                           "Pending"
                            //                                       ? Colors.red
                            //                                       : Colors.green),
                            //                         ),
                            //                       ]),
                            //                 ),
                            //               ],
                            //             ),
                            //           ),
                            //           SizedBox(
                            //             height: 50,
                            //           ),
                            //           FittedBox(
                            //             child: Row(
                            //               children: [
                            //                 Container(
                            //                   width: 270,
                            //                 ),
                            //                 IconButton(
                            //                     icon: Icon(
                            //                         Icons.arrow_circle_down_outlined),
                            //                     color: Colors.white,
                            //                     onPressed: () {
                            //                       showModalBottomSheet(
                            //                           backgroundColor: Colors.white,
                            //                           shape: RoundedRectangleBorder(
                            //                               borderRadius: BorderRadius.only(
                            //                                   topLeft:
                            //                                       Radius.circular(20),
                            //                                   topRight:
                            //                                       Radius.circular(20))),
                            //                           elevation: 20,
                            //                           context: context,
                            //                           builder: (context) {
                            //                             return Column(
                            //                               mainAxisSize: MainAxisSize.min,
                            //                               children: <Widget>[
                            //                                 Padding(
                            //                                   padding:
                            //                                       const EdgeInsets.all(
                            //                                           8.0),
                            //                                   child: Material(
                            //                                     elevation: 5,
                            //                                     borderRadius:
                            //                                         BorderRadius.circular(
                            //                                             30),
                            //                                     color: Colors.orange,
                            //                                     child: MaterialButton(
                            //                                         padding: EdgeInsets
                            //                                             .fromLTRB(20, 15,
                            //                                                 20, 15),
                            //                                         minWidth:
                            //                                             MediaQuery.of(
                            //                                                     context)
                            //                                                 .size
                            //                                                 .width,
                            //                                         onPressed: () async {
                            //                                           await FirebaseFirestore
                            //                                               .instance
                            //                                               .collection(
                            //                                                   "table-book")
                            //                                               .doc(bookingData
                            //                                                   .id)
                            //                                               .update({
                            //                                             'status':
                            //                                                 "In-Progress",
                            //                                           }).then((result) {
                            //                                             Fluttertoast
                            //                                                 .showToast(
                            //                                                     msg:
                            //                                                         "Booking Accepted ");
                            //                                             print("Success!");
                            //                                           }).catchError(
                            //                                                   (error) {
                            //                                             print("Error!");
                            //                                           });
                            //                                         },
                            //                                         child: Text(
                            //                                           "Accept",
                            //                                           textAlign: TextAlign
                            //                                               .center,
                            //                                           style: TextStyle(
                            //                                             fontSize: 16,
                            //                                             color:
                            //                                                 Colors.white,
                            //                                           ),
                            //                                         )),
                            //                                   ),
                            //                                 ),
                            //                                 Padding(
                            //                                   padding:
                            //                                       const EdgeInsets.all(
                            //                                           8.0),
                            //                                   child: Material(
                            //                                     elevation: 5,
                            //                                     borderRadius:
                            //                                         BorderRadius.circular(
                            //                                             30),
                            //                                     color: Colors.blue,
                            //                                     child: MaterialButton(
                            //                                         padding: EdgeInsets
                            //                                             .fromLTRB(20, 15,
                            //                                                 20, 15),
                            //                                         minWidth:
                            //                                             MediaQuery.of(
                            //                                                     context)
                            //                                                 .size
                            //                                                 .width,
                            //                                         onPressed: () {
                            //                                           Navigator.of(
                            //                                                   context)
                            //                                               .pop();
                            //                                           Navigator.pushNamed(
                            //                                               context,
                            //                                               '/chat-to-user-client');
                            //                                           context
                            //                                               .read<
                            //                                                   ChatControllerProvider>()
                            //                                               .setUserClientID(
                            //                                                   userClient.get(
                            //                                                       'id'));
                            //                                         },
                            //                                         child: Text(
                            //                                           "Send Message",
                            //                                           textAlign: TextAlign
                            //                                               .center,
                            //                                           style: TextStyle(
                            //                                             fontSize: 16,
                            //                                             color:
                            //                                                 Colors.white,
                            //                                           ),
                            //                                         )),
                            //                                   ),
                            //                                 ),
                            //                                 Padding(
                            //                                   padding:
                            //                                       const EdgeInsets.all(
                            //                                           8.0),
                            //                                   child: Material(
                            //                                     elevation: 5,
                            //                                     borderRadius:
                            //                                         BorderRadius.circular(
                            //                                             30),
                            //                                     color: Colors.red,
                            //                                     child: MaterialButton(
                            //                                         padding: EdgeInsets
                            //                                             .fromLTRB(20, 15,
                            //                                                 20, 15),
                            //                                         minWidth:
                            //                                             MediaQuery.of(
                            //                                                     context)
                            //                                                 .size
                            //                                                 .width,
                            //                                         onPressed: () async {
                            //                                           // Navigator.of(context)
                            //                                           //     .pop();
                            //                                           // Navigator.pushNamed(
                            //                                           //     context,
                            //                                           //     '/edit-booking-page');
                            //                                           // context
                            //                                           //     .read<
                            //                                           //     BookAGeekProvider>()
                            //                                           //     .setBookingID(
                            //                                           //     userService
                            //                                           //         .get('id'));
                            //                                         },
                            //                                         child: Text(
                            //                                           "Cancel",
                            //                                           textAlign: TextAlign
                            //                                               .center,
                            //                                           style: TextStyle(
                            //                                             fontSize: 16,
                            //                                             color:
                            //                                                 Colors.white,
                            //                                           ),
                            //                                         )),
                            //                                   ),
                            //                                 ),
                            //                                 Padding(
                            //                                   padding:
                            //                                       const EdgeInsets.all(
                            //                                           8.0),
                            //                                   child: Material(
                            //                                     elevation: 5,
                            //                                     borderRadius:
                            //                                         BorderRadius.circular(
                            //                                             30),
                            //                                     color: Colors.green,
                            //                                     child: MaterialButton(
                            //                                         padding: EdgeInsets
                            //                                             .fromLTRB(20, 15,
                            //                                                 20, 15),
                            //                                         minWidth:
                            //                                             MediaQuery.of(
                            //                                                     context)
                            //                                                 .size
                            //                                                 .width,
                            //                                         onPressed: () async {
                            //                                           await FirebaseFirestore
                            //                                               .instance
                            //                                               .collection(
                            //                                                   "table-history")
                            //                                               .doc(
                            //                                                   user!.email)
                            //                                               .set({
                            //                                             "clientName":
                            //                                                 userClient.get(
                            //                                                     'userModel.fullName'),
                            //                                             "userServiceEmail":
                            //                                                 user!.email,
                            //                                             "status":
                            //                                                 "Finished",
                            //                                             "dateFinished": DateFormat
                            //                                                     .yMMMMd()
                            //                                                 .format(DateTime
                            //                                                     .now()),
                            //                                           }).then((value) {
                            //                                             Fluttertoast
                            //                                                 .showToast(
                            //                                                     msg:
                            //                                                         "Transaction Completed");
                            //                                           });
                            //
                            //                                           // Navigator.of(context)
                            //                                           //     .pop();
                            //                                           // Navigator.pushNamed(
                            //                                           //     context,
                            //                                           //     '/edit-booking-page');
                            //                                           // context
                            //                                           //     .read<
                            //                                           //     BookAGeekProvider>()
                            //                                           //     .setBookingID(
                            //                                           //     userService
                            //                                           //         .get('id'));
                            //                                         },
                            //                                         child: Text(
                            //                                           "Finish Transaction",
                            //                                           textAlign: TextAlign
                            //                                               .center,
                            //                                           style: TextStyle(
                            //                                             fontSize: 16,
                            //                                             color:
                            //                                                 Colors.white,
                            //                                           ),
                            //                                         )),
                            //                                   ),
                            //                                 ),
                            //                               ],
                            //                             );
                            //                           });
                            //                     }),
                            //               ],
                            //             ),
                            //           )
                            //         ],
                            //       ),
                            //     ),
                            //   ],
                            // );
                          }),
                    ),
                  ],
                );
              } else {
                return Center(child: Text("No Booking List Found"));
              }

              // return Scaffold(
              //   appBar: AppBar(
              //     backgroundColor: Colors.transparent,
              //     elevation: 0,
              //     leading: IconButton(
              //       icon: Icon(Icons.arrow_back, color: Colors.black),
              //       onPressed: () {
              //         // passing this to our root
              //         Navigator.of(context).pop();
              //       },
              //     ),
              //   ),
              //   body: SingleChildScrollView(
              //       child: Column(
              //     children: [
              //       AspectRatio(
              //         aspectRatio: 3 / 2,
              //         child: Container(
              //           width: double.infinity,
              //           decoration: BoxDecoration(
              //             boxShadow: <BoxShadow>[
              //               BoxShadow(
              //                   color: Colors.black54,
              //                   blurRadius: 15.0,
              //                   offset: Offset(0.0, 0.75))
              //             ],
              //             color: Colors.white60,
              //           ),
              //           child: Column(
              //             mainAxisAlignment: MainAxisAlignment.center,
              //             children: [
              //               Text(currentUser[0]['userModel']['fullName']),
              //             ],
              //           ),
              //         ),
              //       ),
              //       SizedBox(
              //         height: 50.0,
              //       ),
              //       TextButton(
              //         child: Text(
              //           'Edit',
              //         ),
              //         style: TextButton.styleFrom(
              //           primary: Colors.blue,
              //         ),
              //         onPressed: () {
              //           Navigator.pushNamed(context, '/user-client-edit-profile-page');
              //         },
              //       )
              //     ],
              //   )),
              // );
            }),
      ),
    );
  }
}
