import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geekdoctor/Drawer/User-Service-Drawer.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Login-Page.dart';
import 'package:geekdoctor/Provider/User-Service-Login-Register/Controller-User-Service-Provider.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:geekdoctor/model/user_service_model.dart';
import 'package:provider/src/provider.dart';

class UserServiceHome extends StatefulWidget {
  const UserServiceHome({Key? key}) : super(key: key);

  @override
  _UserServiceHomeState createState() => _UserServiceHomeState();
}

class _UserServiceHomeState extends State<UserServiceHome> with WidgetsBindingObserver {
  User? user = FirebaseAuth.instance.currentUser;
  UserServiceProviderModel? loggedInUser = UserServiceProviderModel();
  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();

  loggedIn() async {
    await FirebaseFirestore.instance
        .collection("table-user-service")
        .doc(user!.uid)
        .get()
        .then((value) {
      print(loggedInUser!.fullName);

      this.loggedInUser = UserServiceProviderModel.fromMap(value.data());
      setState(() {});
    });
  }

  @override
  void initState() {
    loggedIn();
    super.initState();

    FirebaseFirestore.instance
        .collection("table-user-service")
        .doc(user!.uid)
        .get()
        .then((value) async {
      print("${user!.uid}" "OKEOEKEKE");

      this.loggedInUser = await UserServiceProviderModel.fromMap(value.data());
      //setState(() {});

      WidgetsBinding.instance?.addObserver(this);
      FirebaseFirestore.instance.collection('table-user-service').doc(user!.uid).update({
        "status": "Active",
      }).then((_) async {});
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    setState(() {
      if (state == AppLifecycleState.resumed) {
        print("RESUME");
        FirebaseFirestore.instance
            .collection('table-user-service')
            .doc(user!.uid)
            .update({
          "status": "Active",
        }).then((_) async {});
      } else if (state == AppLifecycleState.inactive) {
        print("INACTIVE");
        FirebaseFirestore.instance
            .collection('table-user-service')
            .doc(user!.uid)
            .update({
          "status": "Inactive",
        }).then((_) async {});
      } else if (state == AppLifecycleState.detached) {
        print("DETACHED");
        FirebaseFirestore.instance
            .collection('table-user-service')
            .doc(user!.uid)
            .update({
          "status": "Offline",
        }).then((_) async {});
      } else if (state == AppLifecycleState.paused) {
        print("$state: PAUSE");
        FirebaseFirestore.instance
            .collection('table-user-service')
            .doc(user!.uid)
            .update({
          "status": "Inactive",
        }).then((_) async {});
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    FirebaseFirestore.instance.collection('table-user-service').doc(user!.uid).update({
      "status": "Offline",
    }).then((_) async {});
    WidgetsBinding.instance!.removeObserver(this);
    print('dispose called.............');
  }

  DateTime pre_backpress = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(pre_backpress);
        final cantExit = timegap >= Duration(seconds: 2);
        pre_backpress = DateTime.now();
        if (cantExit) {
          //show snackbar

          Fluttertoast.showToast(msg: 'Press Back button again to Exit');
          // final snack = SnackBar(
          //   content: Text('Press Back button again to Exit'),
          //   duration: Duration(seconds: 2),
          // );
          // ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        } else {
          dispose();
          //SystemNavigator.pop();
          // Navigator.pushAndRemoveUntil(
          //     context,
          //     MaterialPageRoute(
          //         builder: (BuildContext context) => new UserLoginClientPage()),
          //     (Route<dynamic> route) => false);
          return true;
        }

        // if (_key.currentState!.isDrawerOpen) {
        //   // Close the drawer when u press back button
        //   Navigator.of(context).pop();
        //
        //   return false;
        // }
        // return Future.value(false); // This line disable to go to Login page
        // //return true;
      },
      child: Scaffold(
          key: _key,
          appBar: AppBar(
            title: Text("Service Provider"),
            backgroundColor: Colors.orange,
            centerTitle: true,
          ),
          drawer: UserServiceDrawer(),

          // body: StreamBuilder(
          //   stream:
          //       context.watch<ControllerUserServiceProvider>().userClientCurrentLoggedIn(),
          //   builder: (context, AsyncSnapshot<QuerySnapshot> snap) {
          //     print(snap.connectionState);
          //     if (snap.connectionState == ConnectionState.active) {
          //       print(snap.data.toString());
          //       if (snap.data == null) {
          //         return Text("not login");
          //       } else {
          //         return Column(
          //           children: [
          //             Text(snap.data?.docs[0]['fullName']),
          //             Text(snap.data?.docs[0]['email']),
          //             ElevatedButton(
          //               style: ElevatedButton.styleFrom(
          //                 primary: Colors.orange, // background
          //                 onPrimary: Colors.white, // foreground
          //               ),
          //               onPressed: () {
          //                 logout(context);
          //               },
          //               child: Text('Logout'),
          //             ),
          //           ],
          //         );
          //       }
          //     } else {
          //       return CircularProgressIndicator();
          //     }
          //   },
          // ),

          body: FutureBuilder(
            future: FirebaseFirestore.instance
                .collection("table-user-service")
                .doc(user!.uid)
                .get(),
            builder: (
              BuildContext context,
              snapshot,
            ) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return const Text('Error');
                } else if (snapshot.hasData) {
                  return Center(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 150,
                              child:
                                  Image.asset("images/geeklogo.png", fit: BoxFit.contain),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text(
                              "Hi" + "  " + "${loggedInUser!.fullName}",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 20),
                            RichText(
                              text: TextSpan(
                                  text: "Welcome to ",
                                  style: TextStyle(fontSize: 16.0, color: Colors.black),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: "Geek Doctor",
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.orange),
                                    ),
                                  ]),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                } else {
                  return const Text('Empty data');
                }
              } else {
                return Text('State: ${snapshot.connectionState}');
              }
            },
          )),
    );
  }
}

// the logout function
Future<void> logout(BuildContext context) async {
  await FirebaseAuth.instance.signOut();

  Navigator.of(context)
      .pushReplacement(MaterialPageRoute(builder: (context) => UserLoginClientPage()));
}
