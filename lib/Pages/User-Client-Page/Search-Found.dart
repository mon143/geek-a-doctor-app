import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geekdoctor/Provider/Bokk-A-Geek/Book-A-Geek.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/Widgets/TextFormField.dart';
import 'package:geekdoctor/Widgets/TextFormFieldDateAndTime.dart';
import 'package:geekdoctor/constant.dart';
import 'package:geekdoctor/model/book_model.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:geekdoctor/model/user_service_model.dart';
import 'package:provider/src/provider.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart';

class SearchFound extends StatefulWidget {
  const SearchFound({Key? key}) : super(key: key);

  @override
  _SearchFoundState createState() => _SearchFoundState();
}

class _SearchFoundState extends State<SearchFound> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _serviceNeedText = TextEditingController();

  DateTime _selectedDate = DateTime.now();
  String _endTime = DateFormat("hh:mm a").format(DateTime.now());
  String _startTime = DateFormat("hh:mm a").format(DateTime.now());

  bool isLoading = false;

  _getDateFromUser() async {
    DateTime? _pickerDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2015),
      lastDate: DateTime(2121),
    );

    if (_pickerDate != null) {
      _selectedDate = _pickerDate;
      context.read<ControllerClientProvider>().setDisplayDate(_selectedDate);
      // setState(() {
      //   _selectedDate = _pickerDate;
      // });
    } else {}
  }

  _showTimePicker() {
    return showTimePicker(
      initialEntryMode: TimePickerEntryMode.input,
      context: context,
      initialTime: TimeOfDay(
          hour: int.parse(_startTime.split(":")[0]),
          minute: int.parse(_startTime.split(":")[1].split(" ")[0])),
    );
  }

  _getTimeFromUser({required bool isStartTime}) async {
    var pickedTime = await _showTimePicker();
    String _formatTime = pickedTime!.format(context);

    // DateTime parsedTime = DateFormat.jm().parse(pickedTime.format(context).toString());
    // //converting to DateTime so that we can further format on different pattern.
    // print(parsedTime); //output 1970-01-01 22:53:00.000
    // String formattedTime = DateFormat('HH:mm:ss').format(parsedTime);
    // print(formattedTime); //output 14:59:00

    if (pickedTime == null) {
      print("Time Canceled");
    } else if (isStartTime) {
      _startTime = _formatTime;
      //context.read<ControllerClientProvider>().setDisplayStartTime(_startTime);

      setState(() {
        _startTime = _formatTime;
      });
    } else if (!isStartTime) {
      _endTime = _formatTime;
      // context.read<ControllerClientProvider>().setDisplayEndTime(_endTime);

      setState(() {
        _endTime = _formatTime;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future<Response> sendNotification(
      List<String> tokenIdList, String contents, String heading) async {
    return await post(
      Uri.parse('https://onesignal.com/api/v1/notifications'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "app_id":
            keyAppID, //kAppId is the App Id that one get from the OneSignal When the application is registered.

        "include_player_ids":
            tokenIdList, //tokenIdList Is the List of All the Token Id to to Whom notification must be sent.

        // android_accent_color reprsent the color of the heading text in the notifiction
        "android_accent_color": "FF9976D2",

        //"small_icon": "ic_stat_onesignal_default",

        "large_icon":
            "https://firebasestorage.googleapis.com/v0/b/geek-doctor-2dd82.appspot.com/o/scaled_image_picker6842236089939337044.png?alt=media&token=d8a07855-98e8-4a26-ac78-876fa2c2e278",

        "headings": {"en": heading},

        "contents": {"en": contents},
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    FirebaseFirestore.instance
        .collection("table-user-client")
        .doc(user!.uid)
        .get()
        .then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());

      print(loggedInUser.fullName);
    });

    return Scaffold(
      body: StreamBuilder(
        stream: context.watch<ControllerClientProvider>().getUserServiceEmail(),
        builder: (context, AsyncSnapshot<QuerySnapshot?> snapshot) {
          final currentUser = snapshot.data?.docs;
          if (snapshot.hasData) {
            return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text("Book A Geek"),
                backgroundColor: Colors.orange,
                elevation: 0,
                // leading: IconButton(
                //   icon: Icon(Icons.arrow_back, color: Colors.black),
                //   onPressed: () {
                //     //Navigator.pushNamed(context, '/geek-a-book-list-page');
                //
                //     // passing this to our root
                //     Navigator.of(context).pop();
                //   },
                // ),
              ),
              body: SingleChildScrollView(
                  child: Column(
                children: [
                  AspectRatio(
                    aspectRatio: 3 / 2,
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.black54,
                              blurRadius: 15.0,
                              offset: Offset(0.0, 0.75))
                        ],
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 150,
                            height: 150,
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: 200,
                                  height: 200,
                                  child: CachedNetworkImage(
                                    width: 50,
                                    height: 50,
                                    fit: BoxFit.cover,
                                    imageUrl: "${currentUser![0]['imageUrl']}",
                                    progressIndicatorBuilder:
                                        (context, url, downloadProgress) =>
                                            CircularProgressIndicator(
                                                value: downloadProgress.progress),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.error,
                                      size: 100,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text("${currentUser[0]['fullName']}"),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Column(
                    children: [
                      Text(
                        "Expertise",
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "${currentUser[0]['skills']['expertise1']}",
                      ),
                      currentUser[0]['skills']['expertise2'] == null
                          ? Text("")
                          : Text(
                              "${currentUser[0]['skills']['expertise2']}",
                            ),
                      currentUser[0]['skills']['expertise3'] == null
                          ? Text("")
                          : Text(
                              "${currentUser[0]['skills']['expertise3']}",
                            ),
                      currentUser[0]['skills']['expertise4'] == null
                          ? Text("")
                          : Text(
                              "${currentUser[0]['skills']['expertise4']}",
                            ),
                      currentUser[0]['skills']['expertise5'] == null
                          ? Text("")
                          : Text(
                              "${currentUser[0]['skills']['expertise5']}",
                            ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        // TextFormField(
                        //   controller: _nameText,
                        //   keyboardType: TextInputType.multiline,
                        //   maxLines: null,
                        //   // textInputAction: TextInputAction.done,
                        //   validator: (value) {
                        //     if (value!.isEmpty) {
                        //       return ("Name is required ");
                        //     }
                        //   },
                        //   decoration: InputDecoration(
                        //     hintText: "Service Need",
                        //     suffixIcon: Icon(Icons.done),
                        //     border: OutlineInputBorder(
                        //       borderRadius: BorderRadius.circular(10),
                        //     ),
                        //   ),
                        // ),

                        SizedBox(
                          height: 4.0,
                        ),

                        Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              InputFieldDesign.inputField(
                                  "Service Need", "Service Need", _serviceNeedText,
                                  widget: null, validator: (value) {
                                if (value!.isEmpty) {
                                  return ("Service is required");
                                }
                              }),
                              InputFieldDesign.inputField(
                                  "Date",
                                  //'${context.watch<ControllerClientProvider>().getDisplayDate}',

                                  DateFormat.yMMMMd().format(_selectedDate),
                                  null,
                                  widget: null,
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.calendar_today_outlined),
                                    color: Colors.grey,
                                    onPressed: () {
                                      _getDateFromUser();
                                    },
                                  ),
                                  validator: (value) {}),
                              Row(
                                children: [
                                  Expanded(
                                    child: InputFieldDesign.inputField(
                                        "Start Time",
                                        //'${context.watch<ControllerClientProvider>().getDisplayStartTime}',
                                        //appState.getDisplayStartTime,
                                        _startTime,
                                        null,
                                        widget: null,
                                        suffixIcon: IconButton(
                                          icon: Icon(Icons.access_time_rounded),
                                          color: Colors.grey,
                                          onPressed: () {
                                            _getTimeFromUser(isStartTime: true);
                                          },
                                        ),
                                        validator: (value) {}),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Expanded(
                                    child: InputFieldDesign.inputField(
                                        "End Time",
                                        //'${context.watch<ControllerClientProvider>().getDisplayEndTime}',

                                        // appState.getDisplayEndTime,
                                        _endTime,
                                        null,
                                        widget: null,
                                        suffixIcon: IconButton(
                                          icon: Icon(Icons.access_time_rounded),
                                          color: Colors.grey,
                                          onPressed: () {
                                            _getTimeFromUser(isStartTime: false);
                                          },
                                        ),
                                        validator: (value) {}),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),

                        SizedBox(
                          height: 40.0,
                        ),
                        // TextButton(
                        //   child: Text(
                        //     'Book',
                        //   ),
                        //   style: TextButton.styleFrom(
                        //     primary: Colors.blue,
                        //   ),
                        //   onPressed: () {
                        //     Navigator.pushNamed(
                        //         context, '/user-client-edit-profile-page');
                        //   },
                        // ),

                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.orange, // background
                            onPrimary: Colors.white, // foreground
                          ),
                          onPressed: () async {
                            BookModel bookModel = BookModel();
                            bookModel.serviceNeed = _serviceNeedText.text;
                            bookModel.dateToBook =
                                DateFormat.yMMMMd().format(_selectedDate);
                            bookModel.startTime = _startTime;
                            bookModel.endTime = _endTime;
                            bookModel.status = "Pending";
                            bookModel.userModel = {
                              "uid": loggedInUser.uid,
                              "fullName": loggedInUser.fullName,
                              "contactNumber": loggedInUser.contactNumber,
                              "email": loggedInUser.email,
                              "clientAddress": loggedInUser.address,
                              "imageUrl": loggedInUser.imageUrl
                            };
                            bookModel.userServiceModel = {
                              "uid": currentUser[0]['uid'],
                              "fullName": currentUser[0]['fullName'],
                              "email": currentUser[0]['email'],
                              "imageUrl": currentUser[0]['imageUrl'],
                            };

                            if (_formKey.currentState!.validate()) {
                              List listAllBooking = [];
                              bool canBook = true;

                              String? _dateNow =
                                  DateFormat.yMMMMd().format(_selectedDate);
                              final res = await FirebaseFirestore.instance
                                  .collection("table-book")
                                  .get();

                              res.docs.forEach((doc) {
                                listAllBooking.add(doc.data());
                              });

                              String sT = _startTime.split(":")[0];
                              String eT = _endTime.split(":")[0];
                              String _startTimeAmPm =
                                  _startTime.substring(5); // current time value
                              String _endTimeAmPm =
                                  _endTime.substring(5); // current time value

                              for (int i = 0; i < listAllBooking.length; i++) {
                                String? dateToBook = listAllBooking[i]['dateToBook'];
                                String? startTime = listAllBooking[i]['startTime'];
                                String? endTime = listAllBooking[i]['endTime'];
                                String? userServiceName =
                                    listAllBooking[i]['userServiceModel']['fullName'];

                                String startTimeValueToDatabase =
                                    startTime!.split(":")[0];
                                int intStartTimeValueToDatabase =
                                    int.parse(startTimeValueToDatabase);
                                String endTimeValueToDatabase = endTime!.split(":")[0];
                                int intEndTimeValueToDatabase =
                                    int.parse(endTimeValueToDatabase);

                                int startST = int.parse(sT);
                                int endET = int.parse(eT);

                                int limitTotalMaxBookTime = startST + endET;

                                //int hour = int.parse(_startTime.substring(0, 1));

                                String startTimeAmPmDB =
                                    startTime.substring(5); //databse time value
                                String endTimeAmPmDB =
                                    endTime.substring(5); //databse time value

                                DateTime parsedEndTime24 = DateFormat.jm().parse(endTime);
                                String formattedTime =
                                    DateFormat('HH:mm a').format(parsedEndTime24);

                                String endTime24 = formattedTime.split(":")[0];
                                int _endTimeDB24 = int.parse(endTime24);

                                DateTime parsedStartTime24 =
                                    DateFormat.jm().parse(startTime);
                                String formattedTimeStart =
                                    DateFormat('HH:mm a').format(parsedStartTime24);

                                String startTime24 = formattedTimeStart.split(":")[0];
                                int _startTimeDB24 = int.parse(startTime24);

                                DateTime _parsedEndTime24 =
                                    DateFormat.jm().parse(_endTime);
                                String _formattedTime =
                                    DateFormat('HH:mm a').format(_parsedEndTime24);

                                String endTime24_ = _formattedTime.split(":")[0];
                                int endSelectedTime24 = int.parse(endTime24_);

                                DateTime _parsedStartTime24 =
                                    DateFormat.jm().parse(_startTime);
                                String _formattedTimeStart =
                                    DateFormat('HH:mm a').format(_parsedStartTime24);

                                String startTime24_ = _formattedTimeStart.split(":")[0];
                                int startSelectedTime24 = int.parse(startTime24_);

                                if (sT == eT) {
                                  canBook = false;
                                  Fluttertoast.showToast(
                                      msg: " Start time and End Time Invalid. ");
                                  break;
                                } else if (currentUser[0]['fullName'] ==
                                        userServiceName &&
                                    dateToBook == _dateNow &&
                                    startSelectedTime24 >= _startTimeDB24 &&
                                    startSelectedTime24 <= _endTimeDB24) {
                                  canBook = false;
                                  //print(currentUser[0]['fullName']);
                                  Fluttertoast.showToast(
                                      toastLength: Toast.LENGTH_LONG,
                                      msg: "${currentUser[0]['fullName']}" +
                                          " "
                                              "is taken from" +
                                          " "
                                              "${startTime}" +
                                          " "
                                              "to" +
                                          " " "${endTime}");
                                  break;
                                } else if (currentUser[0]['fullName'] ==
                                        userServiceName &&
                                    dateToBook == _dateNow &&
                                    startSelectedTime24 <= _startTimeDB24 &&
                                    endSelectedTime24 >= _startTimeDB24) {
                                  canBook = false;

                                  print("${_dateNow}" + "${dateToBook}" + "HEY");
                                  Fluttertoast.showToast(
                                      toastLength: Toast.LENGTH_LONG,
                                      msg: "${currentUser[0]['fullName']}" +
                                          " "
                                              "is taken from" +
                                          " "
                                              "${startTime}" +
                                          " "
                                              "to" +
                                          " " "${endTime}");
                                  break;
                                } else if (currentUser[0]['fullName'] ==
                                        userServiceName &&
                                    dateToBook == _dateNow &&
                                    startSelectedTime24 <= _startTimeDB24 &&
                                    endSelectedTime24 >= _endTimeDB24) {
                                  canBook = false;

                                  Fluttertoast.showToast(
                                      toastLength: Toast.LENGTH_LONG,
                                      msg: "${currentUser[0]['fullName']}" +
                                          " "
                                              "is taken from" +
                                          " "
                                              "${startTime}" +
                                          " "
                                              "to" +
                                          " " "${endTime}");
                                  break;
                                } else if (currentUser[0]['fullName'] ==
                                        userServiceName &&
                                    dateToBook == _dateNow &&
                                    startSelectedTime24 >= endSelectedTime24) {
                                  canBook = false;

                                  Fluttertoast.showToast(
                                      toastLength: Toast.LENGTH_LONG,
                                      msg: "${currentUser[0]['fullName']}" +
                                          " "
                                              "is taken from" +
                                          " "
                                              "${startTime}" +
                                          " "
                                              "to" +
                                          " " "${endTime}");
                                  break;
                                }
                              }

                              if (listAllBooking.length == 0 &&
                                  sT == eT &&
                                  _startTimeAmPm == _endTimeAmPm) {
                                canBook = false;
                                Fluttertoast.showToast(
                                    msg: " Start time and End Time Invalid. ");
                              }

                              if (canBook) {
                                context.read<BookAGeekProvider>().bookingAdd(bookModel);
                                sendNotification(
                                    ["${currentUser[0]['tokenId']}"],
                                    "${loggedInUser.fullName}" + "  is Booking To You.",
                                    "Book A Geek");
                              }

                              //Navigator.pushNamed(context, '/client-home-page');
                            }
                          },
                          child: Text('Book Now'),
                        )
                      ],
                    ),
                  )
                ],
              )),
            );
          }

          return Text("");
        },
      ),
    );
  }
}
