import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geekdoctor/Loading-Indicator/Loading-Indicator.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Register-Page.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/Widgets/TextFormField.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:provider/src/provider.dart';

import 'User-Client-Home-Page.dart';

class UserLoginClientPage extends StatefulWidget {
  const UserLoginClientPage({Key? key}) : super(key: key);

  @override
  _UserLoginClientPageState createState() => _UserLoginClientPageState();
}

class _UserLoginClientPageState extends State<UserLoginClientPage> {
  // string for displaying the error Message
  String? errorMessage;
  bool _isHidden = true;

  // our form key
  final _formKey = GlobalKey<FormState>();
  // editing Controller

  final TextEditingController _emailText = TextEditingController();

  final TextEditingController _passwordText = TextEditingController();

  bool? isL;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("User Client Login"),
        backgroundColor: Colors.orange,
        // actions: [
        //   PopupMenuButton(
        //     itemBuilder: (BuildContext bc) => [
        //       PopupMenuItem(
        //           child: Text("Registration Service Provider"),
        //           value: "/user-service-registration-page"),
        //       PopupMenuItem(
        //           child: Text("Login Service Provider"),
        //           value: "/user-service-login-page")
        //     ],
        //     onSelected: (route) {
        //       print(route.toString());
        //       // Note You must create respective pages for navigation
        //       Navigator.pushNamed(context, route.toString());
        //     },
        //   ),
        // ],
      ),
      body: Center(
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "images/booking.png",
                  width: 200,
                  height: 200,
                ),
                RichText(
                  text: TextSpan(
                      text: "Hi, Welcome to   ",
                      style: TextStyle(fontSize: 16.0, color: Colors.black),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Geek Doctor",
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.orange),
                        ),
                      ]),
                ),
                SizedBox(height: 20.0),
                TextFormFields.textFormFields("Email", "Email", _emailText,
                    widget: null,
                    obscureText: false,
                    sufixIcon: null,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Email is required for login");
                  }
                }),
                SizedBox(
                  height: 15.0,
                ),
                TextFormFields.textFormFields("Password", "Password", _passwordText,
                    widget: null,
                    obscureText: _isHidden,
                    sufixIcon: IconButton(
                      icon: Icon(_isHidden ? Icons.visibility : Icons.visibility_off),
                      onPressed: () {
                        // This is the trick

                        _isHidden = !_isHidden;

                        (context as Element).markNeedsBuild();
                      },
                    ),
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Password is required for login");
                  }
                }),
                SizedBox(
                  height: 40.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Material(
                    elevation: 5,
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.orange,
                    child: MaterialButton(
                        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                        minWidth: MediaQuery.of(context).size.width,
                        onPressed: () async {
                          final u = Provider.of<ControllerClientProvider>(context,
                              listen: false);
                          //
                          // if (!u.isLoadingIndicator) {
                          //   DialogBuilder(context).showLoadingIndicator();
                          // }
                          // else if (context
                          //         .watch<ControllerClientProvider>()
                          //         .isLoadingIndicator ==
                          //     true) {
                          //   DialogBuilder(context).hideOpenDialog;
                          // }

                          if (_formKey.currentState!.validate()) {
                            context
                                .read<ControllerClientProvider>()
                                .signIn(_emailText.text, _passwordText.text, context);
                          }
                        },
                        // ignore: prefer_const_constructors

                        child: Text(
                          "Sign In",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        )),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                  Text("Don't have an account? "),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/user-register-client-page');
                    },
                    child: Text(
                      "SignUp",
                      style: TextStyle(
                          color: Colors.redAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                  )
                ])
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DialogBuilder {
  DialogBuilder(this.context);

  final BuildContext context;

  void showLoadingIndicator() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              backgroundColor: const Color(0xFFFF7606),
              content: LoadingIndicator(),
            ));
      },
    );
  }

  void hideOpenDialog() {
    Navigator.of(context).pop();
  }
}
