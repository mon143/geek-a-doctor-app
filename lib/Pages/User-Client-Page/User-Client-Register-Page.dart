import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/Widgets/TextFormField.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:provider/src/provider.dart';

import 'User-Client-Home-Page.dart';

class UserRegisterClientPage extends StatefulWidget {
  const UserRegisterClientPage({Key? key}) : super(key: key);

  @override
  _UserRegisterClientPageState createState() => _UserRegisterClientPageState();
}

class _UserRegisterClientPageState extends State<UserRegisterClientPage> {
  // string for displaying the error Message
  String? errorMessage;

  // our form key
  final _formKey = GlobalKey<FormState>();
  // editing Controller
  bool _isHidden = true;

  final TextEditingController _nameText = TextEditingController();
  final TextEditingController _emailText = TextEditingController();
  final TextEditingController _addressText = TextEditingController();
  final TextEditingController _contactText = TextEditingController();
  final TextEditingController _passwordText = TextEditingController();
  final TextEditingController _confirmPasswordText = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "User Client Register",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.orange,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            // passing this to our root
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Image.asset(
                //   "images/booking.png",
                //   width: 200,
                //   height: 200,
                // ),
                TextFormFields.textFormFields("Name", "Name", _nameText,
                    widget: null,
                    obscureText: false,
                    sufixIcon: null,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Name is required for login");
                  }
                }),
                SizedBox(
                  height: 12.0,
                ),
                TextFormFields.textFormFields("Email", "Email", _emailText,
                    widget: null,
                    obscureText: false,
                    sufixIcon: null,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Email is required for login");
                  }
                }),
                SizedBox(
                  height: 12.0,
                ),
                TextFormFields.textFormFields("Address", "Address", _addressText,
                    widget: null,
                    obscureText: false,
                    sufixIcon: null,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Address is required for login");
                  }
                }),
                SizedBox(
                  height: 12.0,
                ),
                TextFormFields.textFormFields("Contact", "Contact", _contactText,
                    widget: null,
                    obscureText: false,
                    sufixIcon: null,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Contact is required for login");
                  }
                }),
                SizedBox(
                  height: 12.0,
                ),
                TextFormFields.textFormFields("Password", "Password", _passwordText,
                    widget: null,
                    obscureText: _isHidden,
                    sufixIcon: IconButton(
                      icon: Icon(_isHidden ? Icons.visibility : Icons.visibility_off),
                      onPressed: () {
                        // This is the trick

                        _isHidden = !_isHidden;

                        (context as Element).markNeedsBuild();
                      },
                    ),
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next, validator: (value) {
                  if (value!.isEmpty) {
                    return ("Password is required for login");
                  }
                }),
                SizedBox(
                  height: 12.0,
                ),
                TextFormFields.textFormFields(
                  "Confirm Password",
                  "Password",
                  _confirmPasswordText,
                  widget: null,
                  obscureText: _isHidden,
                  sufixIcon: IconButton(
                    icon: Icon(_isHidden ? Icons.visibility : Icons.visibility_off),
                    onPressed: () {
                      // This is the trick

                      _isHidden = !_isHidden;

                      (context as Element).markNeedsBuild();
                    },
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.done,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return ("Confirm Password is required");
                    } else if (_confirmPasswordText.text != _passwordText.text) {
                      return "Password don't match";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 40.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Material(
                    elevation: 5,
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.orange,
                    child: MaterialButton(
                        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                        minWidth: MediaQuery.of(context).size.width,
                        onPressed: () {
                          UserModel? user = UserModel();
                          user.fullName = _nameText.text;
                          user.email = _emailText.text;
                          user.address = _addressText.text;
                          user.contactNumber = _contactText.text;

                          if (_formKey.currentState!.validate()) {
                            context.read<ControllerClientProvider>().signUp(
                                _emailText.text, _passwordText.text, user, context);
                            //Navigator.pushNamed(context, '/client-home-page');
                          }

                          // Navigator.push(
                          //     (context),
                          //     MaterialPageRoute(
                          //         builder: (context) => UserClientHomePage()));
                        },
                        // ignore: prefer_const_constructors
                        child: Text(
                          "Sign Up",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
