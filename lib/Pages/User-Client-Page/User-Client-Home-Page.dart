import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geekdoctor/Drawer/User-Client-Drawer.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Login-Page.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/Widgets/BottomNavigationBar.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:geekdoctor/model/user_service_model.dart';
import 'package:provider/src/provider.dart';

import 'Geek-A-Book-List.dart';

class UserClientHomePage extends StatefulWidget {
  const UserClientHomePage({Key? key}) : super(key: key);

  @override
  _UserClientHomePageState createState() => _UserClientHomePageState();
}

class _UserClientHomePageState extends State<UserClientHomePage>
    with WidgetsBindingObserver {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();
  DateTime pre_backpress = DateTime.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance?.addObserver(this);
    FirebaseFirestore.instance.collection('table-user-client').doc(user!.uid).update({
      "status": "Active",
    }).then((_) async {});
    print("RESUME");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    setState(() {
      if (state == AppLifecycleState.resumed) {
        print("RESUME");
        FirebaseFirestore.instance.collection('table-user-client').doc(user!.uid).update({
          "status": "Active",
        }).then((_) async {});
      } else if (state == AppLifecycleState.inactive) {
        print("INACTIVE");
        FirebaseFirestore.instance.collection('table-user-client').doc(user!.uid).update({
          "status": "Inactive",
        }).then((_) async {});
      } else if (state == AppLifecycleState.paused) {
        print(" PAUSE");
        FirebaseFirestore.instance.collection('table-user-client').doc(user!.uid).update({
          "status": "Inactive",
        }).then((_) async {});
      } else if (state == AppLifecycleState.detached) {
        print("DETACHED");
        FirebaseFirestore.instance.collection('table-user-client').doc(user!.uid).update({
          "status": "Offline",
        }).then((_) async {});
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
    FirebaseFirestore.instance.collection('table-user-client').doc(user!.uid).update({
      "status": "Offline",
    }).then((_) async {});

    print('dispose called.............');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(pre_backpress);
        final cantExit = timegap >= Duration(seconds: 2);
        pre_backpress = DateTime.now();
        if (cantExit) {
          //show snackbar
          Fluttertoast.showToast(msg: 'Press Back button again to Exit');
          //
          // final snack = SnackBar(
          //   content: Text('Press Back button again to Exit'),
          //   duration: Duration(seconds: 2),
          // );
          // ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        } else {
          dispose();
          //SystemNavigator.pop();
          // Navigator.pushAndRemoveUntil(
          //     context,
          //     MaterialPageRoute(
          //         builder: (BuildContext context) => new UserLoginClientPage()),
          //     (Route<dynamic> route) => false);
          return true;
        }
        // if (_key.currentState!.isDrawerOpen) {
        //   // Close the drawer when u press back button
        //   Navigator.of(context).pop();
        //
        //   return false;
        // }
        // return Future.value(false); // This line disable to go to Login page
        //return true;
      },
      //onWillPop: () => Future.value(false),
      child: Scaffold(
        //backgroundColor: Colors.grey[200],
        key: _key,
        appBar: AppBar(
          centerTitle: true,
          title: Text("Home"),
          backgroundColor: Colors.orange,
        ),
        drawer: UserClientDrawer(),
        bottomNavigationBar: BottomNavBar(index: 0),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "images/geeklogo.png",
                width: 160,
                height: 160,
              ),
              RichText(
                text: TextSpan(
                    text: "Hi, Welcome to  ",
                    style: TextStyle(fontSize: 20.0, color: Colors.black),
                    children: <TextSpan>[
                      TextSpan(
                        text: "Geek Doctor",
                        style: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.orange),
                      ),
                    ]),
              ),
              SizedBox(height: 20.0),
              Container(
                decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.shade500,
                        offset: Offset(5, 5),
                        blurRadius: 10,
                        spreadRadius: 1,
                      ),
                      BoxShadow(
                        color: Colors.grey[300]!,
                        offset: Offset(-2, -2),
                        blurRadius: 10,
                        spreadRadius: 1,
                      ),
                    ]),
                child: TextButton(
                  style: ElevatedButton.styleFrom(
                    //primary: Colors.orange, // background
                    onPrimary: Colors.black, // foreground
                  ),
                  onPressed: () {
                    //Routing.goToPage(GeekABookListServicePage(), context);
                    Navigator.pushNamed(context, '/geek-a-book-list-page');
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('Book A Geek Now'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

    // BottomNavigationBar buildBottomNavigationBar() {
    //   return BottomNavigationBar(
    //     type: BottomNavigationBarType.fixed,
    //     currentIndex: _selectedIndex,
    //     onTap: (value) {
    //       setState(() {
    //         _selectedIndex = value;
    //       });
    //     },
    //     items: [
    //       BottomNavigationBarItem(
    //           icon: IconButton(
    //             icon: Icon(Icons.home),
    //             onPressed: () {},
    //           ),
    //           label: "Home"),
    //       BottomNavigationBarItem(
    //           icon: IconButton(
    //             icon: Icon(Icons.person),
    //             onPressed: () {
    //               Navigator.pushNamed(context, '/user-client-profile-page');
    //             },
    //           ),
    //           label: "Profile"),
    //       BottomNavigationBarItem(
    //           icon: IconButton(
    //             icon: Icon(Icons.history),
    //             onPressed: () {},
    //           ),
    //           label: "History"),
    //       BottomNavigationBarItem(
    //           icon: IconButton(
    //             icon: Icon(Icons.search),
    //             onPressed: () {},
    //           ),
    //           label: "Search"),
    //
    //       // BottomNavigationBarItem(
    //       //   icon: CircleAvatar(
    //       //     radius: 14,
    //       //     backgroundImage: AssetImage("assets/images/user_2.png"),
    //       //   ),
    //       //   label: "Profile",
    //       // ),
    //     ],
    //   );
  }
}
