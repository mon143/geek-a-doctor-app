import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:geekdoctor/Pages/User-Client-Page/Book-A-Geek.dart';
import 'package:geekdoctor/Provider/AppProvider.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/model/user_service_model.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/src/provider.dart';

class GeekABookListServicePage extends StatefulWidget {
  const GeekABookListServicePage({Key? key}) : super(key: key);

  @override
  _GeekABookListServicePageState createState() => _GeekABookListServicePageState();
}

class _GeekABookListServicePageState extends State<GeekABookListServicePage> {
  Position? _currentUserPosition;
  double? distanceImMeter = 0.0;
  double defualtValue = 1.0;
  List listUsers = [];
  String? _currentAddress;

  List<int> res = [];
  List rateStar = [];
  String ratings = "0";
  int max_index = 0;
  int max_value = 0;

  Future getAlluser() async {
    try {
      final res = await FirebaseFirestore.instance.collection("table-user-service").get();

      res.docs.forEach((doc) {
        listUsers.add(doc.data());
      });

      return res;
      print("${listUsers}" "adasdadasdada");
    } catch (e) {
      return null;
    }
  }

  Future getDis() async {
    try {
      _currentUserPosition =
          await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      if (listUsers.length != 0) {
        for (int i = 0; i < listUsers.length; i++) {
          double? storelat = listUsers[i]['position']['latitude'];
          double? storelng = listUsers[i]['position']['longitude'];

          distanceImMeter = await Geolocator.distanceBetween(
            _currentUserPosition!.latitude,
            _currentUserPosition!.longitude,
            storelat!,
            storelng!,
          );
          double? distance = distanceImMeter?.round().toDouble();

          listUsers[i]['distance'] = (distance! / 1000).round().toDouble();
          double? dis = listUsers[i]['distance'];

          // print("${listUsers[i]['position']['latitude'].toString()}" "Distance");
          // print("${_currentUserPosition!.latitude}" "Sincere Distance");

          setState(() {});
        }
      }

      listUsers.sort((a, b) => a["distance"].compareTo(b["distance"]));
    } catch (e) {}

    //getRate();
  }

  Future getRate() async {
    try {
      for (int i = 0; i < listUsers.length; i++) {
        res = [
          listUsers[i]['rating']['rate1'].round(),
          listUsers[i]['rating']['rate2'].round(),
          listUsers[i]['rating']['rate3'].round(),
          listUsers[i]['rating']['rate4'].round(),
          listUsers[i]['rating']['rate5'].round(),
        ];

        max_value = res.reduce(max);

        max_index = res.indexOf(max_value);

        rateStar.add(max_index + 1);

        //listUsers.add(max_index);

        //setState(() {});
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future _getAddressFromLatLng() async {
    try {
      _currentUserPosition =
          await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      if (listUsers.length != 0) {
        for (int i = 0; i < listUsers.length; i++) {
          double? storelat = listUsers[i]['position']['latitude'];
          double? storelng = listUsers[i]['position']['longitude'];

          List<Placemark> placemarks = await placemarkFromCoordinates(
              listUsers[i]['position']['latitude'],
              listUsers[i]['position']['longitude']);

          Placemark place = placemarks[0];

          _currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";

          setState(() {
            listUsers[i]['address'] = _currentAddress;
          });
        }
      }

      print("${_currentAddress}" "Address");
    } catch (e) {
      print("ERROR");
      print(e);
    }
  }

  Future getUserClientDetails() async {
    var snapshots =
        await FirebaseFirestore.instance.collection('table-user-service').get();
    var snapshotDocuments = snapshots.docs;
    List<UserServiceProviderModel> jobs = [];
    for (var docs in snapshotDocuments) {
      double? storelat = docs.data()['position']['latitude'];
      double? storelng = docs.data()['position']['longitude'];

      distanceImMeter = await Geolocator.distanceBetween(
        _currentUserPosition!.latitude,
        _currentUserPosition!.longitude,
        storelat!,
        storelng!,
      );
      double? distance = distanceImMeter?.round().toDouble();

      docs.data()['distance'] = (distance! / 1000)..toStringAsFixed(1);

      print(docs.data()['fullName']);
    }
  }

  @override
  void initState() {
    //AppProviders.disposeAllDisposableProviders(context);
    // FirebaseFirestore.instance.collection("table-user-service").get().then((value) {
    //   value.docs.forEach((result) {
    //     res = [
    //       value.docs.first.get('rating')['rate1'],
    //       value.docs.first.get('rating')['rate2'],
    //       value.docs.first.get('rating')['rate3'],
    //       value.docs.first.get('rating')['rate4'],
    //       value.docs.first.get('rating')['rate5'],
    //     ];
    //
    //     max_value = res.reduce(max);
    //
    //     max_index = res.indexOf(max_value);
    //     //setState(() {});
    //   });
    // });
    super.initState();

    //getUserClientDetails();
    getAlluser();
    _getAddressFromLatLng();
    getDis();
  }

  @override
  void setState(VoidCallback fn) {
    // TODO: implement setState
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    getRate();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Services Provider"),
        backgroundColor: Colors.orange,
        elevation: 5,
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back, color: Colors.red),
        //   onPressed: () {
        //     // passing this to our root
        //     Navigator.of(context).pop();
        //   },
        // ),
      ),
      body: FutureBuilder(
        future: getDis(),
        builder: (context, projectSnap) {
          // if (projectSnap.connectionState == ConnectionState.waiting) {
          //   return Center(child: CircularProgressIndicator(color: Colors.black));
          // }

          if (listUsers.length == 0) {
            //print('project snapshot data is: ${projectSnap.data}');
            return Center(
                child: CircularProgressIndicator(
              color: Colors.orange,
            ));
          } else {
            return ListView.builder(
              itemCount: listUsers.length,
              itemBuilder: (context, index) {
                return Card(
                  margin: EdgeInsets.all(12),
                  child: ListTile(
                    leading: Hero(
                      tag: listUsers[index]['uid'],
                      child: CachedNetworkImage(
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover,
                        imageUrl: listUsers[index]['imageUrl'],
                        progressIndicatorBuilder: (context, url, downloadProgress) =>
                            CircularProgressIndicator(value: downloadProgress.progress),
                        errorWidget: (context, url, error) => Icon(
                          Icons.error,
                          size: 100,
                          color: Colors.red,
                        ),
                      ),
                    ),
                    //title: Text(listUsers[index]['fullName']),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          listUsers[index]['fullName'],
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: Colors.black),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(listUsers[index]['address'] == null
                            ? "Philippines"
                            : listUsers[index]['address']),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          listUsers[index]['vaccinated'].toString() == "Yes"
                              ? "Vaccinated"
                              : "Unvaccinated",
                          style: TextStyle(
                            fontSize: 12,
                            color: listUsers[index]['vaccinated'].toString() == "Yes"
                                ? Colors.green
                                : Colors.redAccent,
                          ),
                        ),
                        Divider(
                          thickness: 1,
                        ),
                        Wrap(
                          alignment: WrapAlignment.center,
                          direction: Axis.vertical,
                          children: [
                            Text(listUsers[index]['skills']['expertise1']),
                            listUsers[index]['skills']['expertise2'] == ""
                                ? Container(
                                    height: 0,
                                  )
                                : Text(listUsers[index]['skills']['expertise2']),
                            listUsers[index]['skills']['expertise3'] == ""
                                ? Container(
                                    height: 0,
                                  )
                                : Text(listUsers[index]['skills']['expertise3']),
                            listUsers[index]['skills']['expertise4'] != null
                                ? Text(listUsers[index]['skills']['expertise4'])
                                : Container(),
                            listUsers[index]['skills']['expertise5'] != null
                                ? Text(listUsers[index]['skills']['expertise5'])
                                : Container(),
                          ],
                        ),
                      ],
                    ),
                    trailing: SizedBox(
                      width: 100,
                      child: Column(
                        children: [
                          Expanded(
                            child: Text("${listUsers[index]['distance']}"
                                'KM Away'),
                          ),
                          RatingBarIndicator(
                            rating: double.parse(rateStar[index].toString()),
                            //rating: 3,
                            itemBuilder: (context, index) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            itemCount: 5,
                            itemSize: 20.0,
                            direction: Axis.horizontal,
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) =>
                      //         BookAGeek(userServiceEmail: listUsers[index]['email']),
                      //   ),
                      // );
                      Navigator.pushNamed(context, '/book-a-geek-page');
                      context
                          .read<ControllerClientProvider>()
                          .setUserServiceEmail(listUsers[index]['email']);
                    },
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
