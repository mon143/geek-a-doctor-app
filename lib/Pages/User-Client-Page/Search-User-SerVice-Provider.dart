import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:geekdoctor/Widgets/BottomNavigationBar.dart';
import 'package:geekdoctor/model/user_client_model.dart';
import 'package:provider/src/provider.dart';

import '../../Router.dart';
import 'Book-A-Geek.dart';
import 'Search-Found.dart';

class SearchUserServiceProvider extends StatefulWidget {
  const SearchUserServiceProvider({Key? key}) : super(key: key);

  @override
  _SearchUserServiceProviderState createState() => _SearchUserServiceProviderState();
}

class _SearchUserServiceProviderState extends State<SearchUserServiceProvider> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  String searchKeyword = "";

  List _users = [];
  final List<Map<String, dynamic>> _allUsers = [];
  List<Map<String, dynamic>> _foundUsers = [];

  List<int> res = [];
  List rateStar = [];
  String ratings = "0";
  int max_index = 0;
  int max_value = 0;

  getAlluser() async {
    final res = await FirebaseFirestore.instance.collection("table-user-service").get();

    res.docs.forEach((doc) {
      _allUsers.add(doc.data());
    });
  }

  Future getRate() async {
    try {
      for (int i = 0; i < _allUsers.length; i++) {
        res = [
          _allUsers[i]['rating']['rate1'].round(),
          _allUsers[i]['rating']['rate2'].round(),
          _allUsers[i]['rating']['rate3'].round(),
          _allUsers[i]['rating']['rate4'].round(),
          _allUsers[i]['rating']['rate5'].round(),
        ];

        max_value = res.reduce(max); //get the max value of each data in list

        max_index = res.indexOf(max_value); //get the index of max value in list

        rateStar.add(max_index + 1); // getting the highest  rate star

        //listUsers.add(max_index);

        //setState(() {});
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  void initState() {
    getAlluser();

    _foundUsers = _allUsers;

    super.initState();

    print(_foundUsers);
  }

  // onSearch(String search) {
  //   setState(() {});
  //
  //   FirebaseFirestore.instance
  //       .collection("table-user-service")
  //       .doc(user!.uid)
  //       .get()
  //       .then((value) {
  //     this.loggedInUser = UserModel.fromMap(value.data());
  //
  //     print(loggedInUser.fullName);
  //     setState(() {});
  //
  //   });
  // }

  // This function is called whenever the text field changes
  void _runFilter(String enteredKeyword) {
    List<Map<String, dynamic>> results1 = [];
    List<Map<String, dynamic>> results2 = [];
    List<Map<String, dynamic>> results3 = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      //results = _allUsers;

    } else {
      // results = _allUsers
      //     .where((user) =>
      //         user["fullName"].toLowerCase().contains(enteredKeyword.toLowerCase()))
      //     .toList();

      results1 = _allUsers
          .where((user) => user["skills"]['expertise1']
              .toLowerCase()
              .contains(enteredKeyword.toLowerCase()))
          .toList();

      results2 = _allUsers
          .where((user) => user["skills"]['expertise2']
              .toLowerCase()
              .contains(enteredKeyword.toLowerCase()))
          .toList();

      results3 = _allUsers
          .where((user) => user["skills"]['expertise3']
              .toLowerCase()
              .contains(enteredKeyword.toLowerCase()))
          .toList();

      for (int i = 0; i < results1.length; i++) {
        res = [
          results1[i]['rating']['rate1'].round(),
          results1[i]['rating']['rate2'].round(),
          results1[i]['rating']['rate3'].round(),
          results1[i]['rating']['rate4'].round(),
          results1[i]['rating']['rate5'].round(),
        ];

        max_value = res.reduce(max);

        max_index = res.indexOf(max_value);

        rateStar.add(max_index + 1);

        //listUsers.add(max_index);

        setState(() {});
      }

      for (int i = 0; i < results1.length; i++) {
        res = [
          results1[i]['rating']['rate1'].round(),
          results1[i]['rating']['rate2'].round(),
          results1[i]['rating']['rate3'].round(),
          results1[i]['rating']['rate4'].round(),
          results1[i]['rating']['rate5'].round(),
        ];

        max_value = res.reduce(max);

        max_index = res.indexOf(max_value);

        rateStar.add(max_index + 1);

        //listUsers.add(max_index);

        //setState(() {});
      }

      // results1
      //     .sort((a, b) => a["skills"]['expertise1'].compareTo(b["skills"]['expertise1']));

      // results2
      //     .sort((a, b) => a["skills"]['expertise2'].compareTo(b["skills"]['expertise2']));
      //
      // results3
      //     .sort((a, b) => a["skills"]['expertise3'].compareTo(b["skills"]['expertise3']));

      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      searchKeyword = enteredKeyword;
      _foundUsers = results1;

      if (results1.isNotEmpty) {
        _foundUsers = results1;
      } else if (results2.isNotEmpty) {
        _foundUsers = results2;
      } else {
        _foundUsers = results3;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //getRate();
    return WillPopScope(
      onWillPop: () async {
        context.read<ControllerClientProvider>().setSelectedBottomNav(0);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: Container(
            height: 38,
            child: TextField(
              onChanged: (value) => _runFilter(value),
              decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.all(0),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey.shade500,
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide.none),
                  hintStyle: TextStyle(fontSize: 14, color: Colors.grey.shade500),
                  hintText: "Search..."),
            ),
          ),
        ),
        bottomNavigationBar: BottomNavBar(index: 4),
        body: Column(
          children: [
            Expanded(
                child: (_foundUsers.isNotEmpty)
                    ? ListView.builder(
                        itemCount: _foundUsers.length,
                        itemBuilder: (context, index) {
                          return Card(
                            elevation: 4,
                            margin: EdgeInsets.all(10),
                            child: ListTile(
                              leading: CachedNetworkImage(
                                width: 50,
                                height: 50,
                                fit: BoxFit.cover,
                                imageUrl: _foundUsers[index]['imageUrl'],
                                progressIndicatorBuilder:
                                    (context, url, downloadProgress) =>
                                        CircularProgressIndicator(
                                            value: downloadProgress.progress),
                                errorWidget: (context, url, error) => Icon(
                                  Icons.error,
                                  size: 100,
                                  color: Colors.red,
                                ),
                              ),
                              title: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    _foundUsers[index]['fullName'],
                                    style: TextStyle(
                                        fontSize: 15, fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    _foundUsers[index]['skills']['expertise1'],
                                    style: TextStyle(fontSize: 14),
                                  ),
                                  Text(
                                    _foundUsers[index]['skills']['expertise2'],
                                    style: TextStyle(fontSize: 14),
                                  ),
                                  Text(
                                    _foundUsers[index]['skills']['expertise3'],
                                    style: TextStyle(fontSize: 14),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                          width: 100,
                                          child:
                                              Divider(thickness: 1, color: Colors.grey)),
                                      RatingBarIndicator(
                                        rating: double.parse(rateStar[index].toString()),

                                        //rating: 3,
                                        itemBuilder: (context, index) => Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                        ),
                                        itemCount: 5,
                                        itemSize: 15.0,
                                        direction: Axis.horizontal,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                              subtitle: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(_foundUsers[index]['address']),
                                ],
                              ),
                              trailing: SizedBox(
                                width: 100,
                                child: Row(
                                  children: [],
                                ),
                              ),
                              onTap: () {
                                NavigateRoute.gotoPage(context, BookAGeekPage());
                                context
                                    .read<ControllerClientProvider>()
                                    .setUserServiceEmail(_foundUsers[index]['email']);
                              },
                            ),
                          );
                        })
                    : (searchKeyword.isNotEmpty)
                        ? Center(child: Text("No Results Found"))
                        : Text("")),
          ],
        ),
      ),
    );
  }
}
