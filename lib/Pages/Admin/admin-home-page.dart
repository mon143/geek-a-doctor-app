import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Login-Page.dart';
import 'package:geekdoctor/Pages/User-Service-Page/User-Service-Login.dart';
import 'package:geekdoctor/Pages/User-Service-Page/User-Service-Register-Page.dart';

class AdminHomePage extends StatefulWidget {
  const AdminHomePage({Key? key}) : super(key: key);

  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();

    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => UserServiceLoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
          child: Container(
        child: ActionChip(
            label: Text("  Logout  "),
            onPressed: () async {
              logout(context);
            }),
      )),
    );
  }
}
