import 'package:flutter/material.dart';
import 'package:geekdoctor/Pages/Chat-Page/Chat-User-List-Page.dart';
import 'package:geekdoctor/Pages/User-Client-Page/Search-User-SerVice-Provider.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-History-Page.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Home-Page.dart';
import 'package:geekdoctor/Pages/User-Client-Page/User-Client-Profile-Page.dart';
import 'package:geekdoctor/Provider/User-Client-Login-Register/Controller-Client-Provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/src/provider.dart';

class BottomNavBar extends StatefulWidget {
  final int index;

  const BottomNavBar({required this.index});

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: BottomNavigationBar(
        backgroundColor: Colors.grey[100],
        selectedIconTheme: IconThemeData(color: Colors.orange),
        selectedItemColor: Colors.orange,

        type: BottomNavigationBarType.fixed,
        //currentIndex: Provider.of<ControllerClientProvider>(context).getSelectedBottomNav,
        //currentIndex: context.watch<ControllerClientProvider>().getSelectedBottomNav,
        currentIndex: widget.index,
        onTap: (value) async {
          //   context.read<ControllerClientProvider>().setSelectedBottomNav(value);
          // setState(() {
          //   print('${value} ' + 'KOKEKE');
          // });
        },

        items: [
          BottomNavigationBarItem(
              icon: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[500]!,
                        offset: const Offset(8, 4),
                        blurRadius: 12,
                        spreadRadius: -3,
                      )
                    ]),
                child: IconButton(
                  icon: Icon(Icons.home),
                  onPressed: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => UserClientHomePage()));
                    context.read<ControllerClientProvider>().setSelectedBottomNav(0);
                    // Navigator.pushNamed(context, '/client-home-page');
                  },
                ),
              ),
              backgroundColor: Colors.orange,
              label: "Home"),

          BottomNavigationBarItem(
              icon: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[500]!,
                        offset: const Offset(8, 4),
                        blurRadius: 12,
                        spreadRadius: -3,
                      )
                    ]),
                child: IconButton(
                  icon: Icon(Icons.person),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => UserClientProfilePage()));
                    //context.read<ControllerClientProvider>().setSelectedBottomNav(1);
                    // Navigator.pushNamed(context, '/user-client-profile-page');
                  },
                ),
              ),
              label: "Profile"),

          BottomNavigationBarItem(
              icon: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[500]!,
                        offset: const Offset(8, 4),
                        blurRadius: 12,
                        spreadRadius: -3,
                      )
                    ]),
                child: IconButton(
                  icon: Icon(Icons.message),
                  onPressed: () {
                    // Navigator.push(
                    //     context, MaterialPageRoute(builder: (_) => ChatUserListPage()));
                    Navigator.pushNamed(context, '/booking-list-page');
                    //context.read<ControllerClientProvider>().setSelectedBottomNav(2);
                    // Navigator.pushNamed(context, '/user-client-profile-page');
                  },
                ),
              ),
              label: "Booking"),

          BottomNavigationBarItem(
              icon: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[500]!,
                        offset: const Offset(8, 4),
                        blurRadius: 12,
                        spreadRadius: -3,
                      )
                    ]),
                child: IconButton(
                  icon: Icon(Icons.history),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => UserClientHistoryPage()));
                    //context.read<ControllerClientProvider>().setSelectedBottomNav(3);
                    // Navigator.pushNamed(context, '/user-client-history-page');
                  },
                ),
              ),
              label: "History"),

          BottomNavigationBarItem(
              icon: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[500]!,
                        offset: const Offset(8, 4),
                        blurRadius: 12,
                        spreadRadius: -3,
                      )
                    ]),
                child: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => SearchUserServiceProvider()));
                    //context.read<ControllerClientProvider>().setSelectedBottomNav(4);
                    // Navigator.pushNamed(context, '/search-user-service-provider-page');
                  },
                ),
              ),
              label: "Search"),

          // BottomNavigationBarItem(
          //   icon: CircleAvatar(
          //     radius: 14,
          //     backgroundImage: AssetImage("assets/images/user_2.png"),
          //   ),
          //   label: "Profile",
          // ),
        ],
      ),
    );
  }
}
