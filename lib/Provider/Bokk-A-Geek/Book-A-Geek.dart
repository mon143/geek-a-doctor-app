import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_clean_calendar/clean_calendar_event.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geekdoctor/Abstract/Disposal-Value-Provider.dart';
import 'package:geekdoctor/model/book_model.dart';
import 'package:uuid/uuid.dart';

class BookAGeekProvider extends DisposableProvider {
  bool _isLoading = false;

  bool isRateStar = false;
  bool rateFce = false;
  Icon? iconFace;
  double initialRating = 0.0;

  String? id;

  List<CleanCalendarEvent> _selectedEvents = [];
  setCalendarDate(List<CleanCalendarEvent> date) {
    _selectedEvents = date;
    notifyListeners();
  }

  List<CleanCalendarEvent> get getCalendarDate => _selectedEvents;

  String selectedDate = "";
  setSelectedDate(String date) {
    selectedDate = date;
    notifyListeners();
  }

  String get getSelectedDate => selectedDate;

  setInitialRating(double initial) {
    initialRating = initial;
    notifyListeners();
  }

  double get getInitialRating => initialRating;

  setIconFace(Icon icon) {
    iconFace = icon;
    notifyListeners();
  }

  Icon get getIconFace => iconFace!;

  setRateFace(bool rate) {
    rateFce = rate;
    notifyListeners();
  }

  bool get getRateFace => rateFce;

  setRateStar(bool rate) {
    isRateStar = rate;
    notifyListeners();
  }

  bool get getRateStar => isRateStar;

  setBookingID(String? _id) {
    id = _id;
    notifyListeners();
  }

  Stream<QuerySnapshot> getBooking() {
    return FirebaseFirestore.instance
        .collection("table-book")
        .where('id', isEqualTo: id)
        .snapshots();
  }

  String get bookId => id!;

  bookingAdd(BookModel bookModel) async {
    User? user = FirebaseAuth.instance.currentUser;
    String? id = Uuid().v4();
    //
    // bookModel.serviceNeed = bookModel.serviceNeed;
    // bookModel.dateToBook = bookModel.dateToBook;
    // bookModel.startTime = bookModel.startTime;
    // bookModel.endTime = bookModel.endTime;
    // bookModel.userModel = bookModel.userModel;
    // bookModel.userServiceModel = bookModel.userServiceModel;

    await FirebaseFirestore.instance.collection("table-book").add({
      'id': id,
      'serviceNeed': bookModel.serviceNeed,
      'dateToBook': bookModel.dateToBook,
      'startTime': bookModel.startTime,
      'endTime': bookModel.endTime,
      'status': bookModel.status,
      'userModel': bookModel.userModel,
      'userServiceModel': bookModel.userServiceModel,
    }).then((result) {
      _isLoading = true;
      notifyListeners();
      Fluttertoast.showToast(msg: "Booking created successfully ");
      print("Success!");
    }).catchError((error) {
      print("Error!");
    });
  }

  bookingUpdate(BookModel bookModel, String? id) async {
    await FirebaseFirestore.instance.collection("table-book").doc(id!).update({
      'serviceNeed': bookModel.serviceNeed,
      'dateToBook': bookModel.dateToBook,
      'startTime': bookModel.startTime,
      'endTime': bookModel.endTime,
    }).then((result) {
      notifyListeners();
      Fluttertoast.showToast(msg: "Booking created successfully ");
      print("Success!");
    }).catchError((error) {
      print("Error!");
    });
  }

  bool get isLoading => _isLoading;

  Stream<QuerySnapshot> bookingList() {
    User? user = FirebaseAuth.instance.currentUser;
    return FirebaseFirestore.instance
        .collection("table-book")
        .where('userModel.uid', isEqualTo: user!.uid)
        .orderBy('dateToBook', descending: true)
        .snapshots();
  }

  // Stream<QuerySnapshot> bookingList() {
  //   User? user = FirebaseAuth.instance.currentUser;
  //   return FirebaseFirestore.instance
  //       .collection("table-book")
  //       .doc(user!.uid)
  //       .collection("booksList")
  //       .snapshots();
  // }

  uploadImage(String? uid, String? fileName, String? imageUrl, File? imageFile) async {
    FirebaseStorage storage = FirebaseStorage.instance;
    try {
      Reference ref = storage.ref().child(fileName!);

      UploadTask? uploadTask = ref.putFile(imageFile!);

      await uploadTask.whenComplete(() async {
        imageUrl = await ref.getDownloadURL();
      });

      await FirebaseFirestore.instance.collection('table-user-service').doc(uid).update({
        "imageUrl": imageUrl,
      }).then((_) async {
        //This function will update the table book
        // with field image URL to User Service Provider

        await FirebaseFirestore.instance
            .collection("table-book")
            .where("userServiceModel.uid", isEqualTo: uid)
            .get()
            .then((result) {
          result.docs.forEach((result) {
            print(result.id);

            FirebaseFirestore.instance.collection('table-book').doc(result.id).update({
              "userServiceModel.imageUrl": imageUrl,
            }).then((_) {
              Fluttertoast.showToast(msg: "Image Save");
            });
          });
        });

        print("success!");
      });
    } on FirebaseException catch (error) {
      // print(error);
      //print(downloadUrl + "adasdadasdsadas");
    }
  }

  @override
  void disposeValues() {
    selectedDate = "";
    isRateStar = false;
  }
}
